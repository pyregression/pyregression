from pyregression.regressors import MLR, KNN, RFO, SVR
from pyregression.datasets import load_soilmoisture

import plotly
import plotly.graph_objects as go

# to save the models
import joblib

X,y = load_soilmoisture() 

# make the regressions (test_size = 0.6 by default)
mlr = MLR(X,y)
knn = KNN(X,y)
rfo = RFO(X,y)
svr = SVR(X,y)

# visualize the prediction
fig = go.Figure()
fig.add_trace(go.Scattergl(y = y['vol_water_content'],  opacity=0.7,mode='markers', name=f"y=Water content"))
fig.add_trace(go.Scattergl(y = mlr["y_pred"],           opacity=0.7,mode='markers', name=f"MLR predicted"))
fig.add_trace(go.Scattergl(y = knn["y_pred"],           opacity=0.7,mode='markers', name=f"KNN predicted"))
fig.add_trace(go.Scattergl(y = rfo["y_pred"],           opacity=0.7,mode='markers', name=f"RFO predicted"))
fig.add_trace(go.Scattergl(y = svr["y_pred"],           opacity=0.7,mode='markers', name=f"SVR predicted"))

fig.update_layout(title="Water Content and Raw values of low-cost sensors", xaxis_title="index value",yaxis_title="Volumetric Water content (cm3/cm3)")
fig.show()

# show RMSE and R2 for each regression method & save the models
for reg_method in (mlr, knn, rfo, svr):
    print (reg_method["name"], f"R2 = {reg_method['R2']} | RMSE={reg_method['RMSE']}")
    joblib.dump(reg_method["model"], f"model_{reg_method['name']}.joblib")