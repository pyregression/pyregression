from pyregression.datasets import load_soilmoisture

import plotly
import plotly.graph_objects as go

# to save the models
import joblib

# load dataset example
X,y = load_soilmoisture() 
y = y.values.ravel()

# load saved models
mlr = joblib.load('model_MLR.joblib')
knn = joblib.load('model_KNN.joblib')
rfo = joblib.load('model_RFO.joblib')
svr = joblib.load('model_SVR.joblib')

# make the predictions
y_pred_mlr = mlr.predict(X).ravel()
y_pred_knn = knn.predict(X).ravel()
y_pred_rfo = rfo.predict(X)
y_pred_svr = svr.predict(X)

# show differents predictions
fig = go.Figure()
fig.add_trace(go.Scattergl(y=y,  opacity=0.7,mode='markers', name=f"y=Water content"))
fig.add_trace(go.Scattergl(y=y_pred_mlr, opacity=0.7,mode='markers', name=f"MLR predicted"))
fig.add_trace(go.Scattergl(y=y_pred_knn, opacity=0.7,mode='markers', name=f"KNN predicted"))
fig.add_trace(go.Scattergl(y=y_pred_rfo, opacity=0.7,mode='markers', name=f"RFO predicted"))
fig.add_trace(go.Scattergl(y=y_pred_svr, opacity=0.7,mode='markers', name=f"SVR predicted"))
fig.update_layout(title="Water Content and Raw values of low-cost sensors", xaxis_title="index value",yaxis_title="Volumetric Water content (cm3/cm3)")
fig.show()

# scatter plots
fig = go.Figure()
fig.add_trace(go.Scattergl(x=y_pred_mlr, y=y, opacity=0.7,mode='markers', name=f"MLR "))
fig.add_trace(go.Scattergl(x=y_pred_knn, y=y, opacity=0.7,mode='markers', name=f"KNN "))
fig.add_trace(go.Scattergl(x=y_pred_rfo, y=y, opacity=0.7,mode='markers', name=f"RFO "))
fig.add_trace(go.Scattergl(x=y_pred_svr, y=y, opacity=0.7,mode='markers', name=f"SVR "))
fig.update_layout(title="Water Content vs predicted values of low-cost sensors", xaxis_title="Predicted values",yaxis_title="Volumetric Water content (cm3/cm3)")
fig.show()

# box plots
fig = go.Figure()
fig.add_trace(go.Box(y=y_pred_mlr, name="MLR"))
fig.add_trace(go.Box(y=y_pred_knn, name="KNN"))
fig.add_trace(go.Box(y=y_pred_rfo, name="RFO"))
fig.add_trace(go.Box(y=y_pred_svr, name="SVR"))
fig.add_trace(go.Box(y=y, name="WC"))
fig.update_layout(title="Box of different regression methods", xaxis_title="Predicted values",yaxis_title="Volumetric Water content (cm3/cm3)")
fig.show()

